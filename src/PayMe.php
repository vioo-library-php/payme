<?php

namespace Vioo\PayMe;

class PayMe {

    protected $client_id;
    protected $client_secret;
    protected $signing_key_id;
    protected $signing_key;
    protected $api_base_url; 

    protected $live = false;

    protected $access_token;

    protected $expiry_time;

    protected $cache_file_path;

    protected $log_file_path;
    const VERSION = "0.12";

    /**
     * @param array $config Set the credentials
     * - client_id, client_secret, signing_key_id, signing_key: provided by PayMe
     * - live: set API environment
     */
    public function __construct($config = array()) {
        $this->client_id = $config["client_id"];
        $this->client_secret = $config["client_secret"];
        $this->signing_key_id = $config["signing_key_id"];
        $this->signing_key = $config["signing_key"];
        $this->live = $config["live"];
        $this->api_base_url = $this->live === true ? "https://api.payme.hsbc.com.hk":"https://sandbox.api.payme.hsbc.com.hk";
        $this->cache_file_path = isset($config["cache"]) ? $config["cache"]:false;
        if(isset($config["cache"])) {
            $this->log_file_path = $config["cache"].DIRECTORY_SEPARATOR."payme_log.log";
        } else {
            $this->log_file_path = false;
        }
    }

    public function getAccessToken() {
        $cache_file = $this->cache_file_path.DIRECTORY_SEPARATOR."payme_cache";
        if(file_exists($cache_file) === true) {
            $decoded_file = json_decode(file_get_contents($cache_file), true);
            $pre_expiry_time = new \DateTime($decoded_file["expiresOn"]);
            $pre_expiry_time = $pre_expiry_time->setTimezone(new \DateTimeZone("Asia/Hong_Kong"));
            if($pre_expiry_time->format("Y-m-d H:i:s") > date("Y-m-d H:i:s")) {
                $this->access_token = $decoded_file["accessToken"];
                return ["result" => true, "errors" => []];
            }
            if(isset($decoded_file["editing"])) {
                if(date("Y-m-d H:i:s", strtotime("-1 minutes")) < $decoded_file["refreshTime"]) {
                    return ["result" => false, "errors" => [ [ "errorCode" => "RT001", "errorDescription" => "Please try again." ] ]];
                }
            }
            $decoded_file["editing"] = true;
            $decoded_file["refreshTime"] = date("Y-m-d H:i:s");
            file_put_contents($cache_file, json_encode($decoded_file), LOCK_EX);
        }
        $result = false;
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL               => $this->api_base_url."/oauth2/token",
            CURLOPT_RETURNTRANSFER    => true,
            CURLOPT_ENCODING          => "",
            CURLOPT_MAXREDIRS         => 10,
            CURLOPT_TIMEOUT           => 0,
            CURLOPT_FOLLOWLOCATION    => true,
            CURLOPT_HTTP_VERSION      => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST     => "POST",
            CURLOPT_POSTFIELDS        => "client_id=$this->client_id&client_secret=$this->client_secret",
            CURLOPT_HTTPHEADER        => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Accept: application/json",
                "Api-Version: ".self::VERSION
            ),
        ));
        
        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        $decoded_response = array();
        if($response) {
            $this->logging($response, "getAccessToken");
            $decoded_response = json_decode($response, true);
            file_put_contents($cache_file, json_encode($decoded_response), LOCK_EX);
            $this->access_token = $decoded_response["accessToken"];
        }
        if($error) {
            $this->logging($error, "getAccessToken");
        }
        return [
            "result" => empty($error) && !isset($decoded_response["errors"]), 
            "data" => $decoded_response, 
            "errors" => isset($decoded_response["errors"]) ? $decoded_response["errors"]:null,
            "server_error" => $error
        ]; 
    }

    public function createPayment($config = array()) {
        $result = false;

        $traceId =  substr(uniqid(rand(), false), 4, 8) . "-" . 
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 12);

        $merchantData = isset($config["merchantData"]) ? $config["merchantData"]:array();
        $post_data = array(
            "effectiveDuration" => isset($config["effectiveDuration"]) ? $config["effectiveDuration"]:600,
            "totalAmount"       => $config["totalAmount"],
            "currencyCode"      => isset($config["currencyCode"]) ? $config["currencyCode"]:"HKD",
            "notificationUri"   => isset($config["notificationUri"]) ? $config["notificationUri"]:null,
            "appFailCallback"   => isset($config["appFailCallback"]) ? $config["appFailCallback"]:null,
            "appSuccessCallback"   => isset($config["appSuccessCallback"]) ? $config["appSuccessCallback"]:null,
            "merchantData"      => $merchantData
        );

        $digest = "SHA-256=".base64_encode(hash("sha256", json_encode($post_data), true));
        $request_time = gmdate("Y-m-d\TH:i:s\Z");
        
        $headers = array(
            "(request-target)"  => "post /payments/paymentrequests",
            "Api-Version"       => self::VERSION,
            "Request-Date-Time" => $request_time,
            "Trace-Id"          => $traceId, // user id + company id
            "Authorization"     => "Bearer $this->access_token",
            "Digest"            => $digest
        );
        $signature = $this->generateSignature($headers);


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $this->api_base_url."/payments/paymentrequests",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => json_encode($post_data),
            CURLOPT_HTTPHEADER      => array(
                "Api-Version: ".self::VERSION,
                "Content-Type: application/json",
                "Accept: application/json",
				"Accept-Language: en-US",
                "Trace-Id: $traceId",
                "Request-Date-Time: $request_time",
                "Signature: $signature",
                "Digest: $digest",
                "Authorization: Bearer $this->access_token"
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        $decoded_response = array();
        if($response) {
            $this->logging($response, "createPayment");
            $decoded_response = json_decode($response, true);
        }
        if($error) {
            $this->logging($error, "createPayment");
        }
        return [
            "result" => empty($error) && !isset($decoded_response["errors"]), 
            "data" => $decoded_response, 
            "errors" => isset($decoded_response["errors"]) ? $decoded_response["errors"]:null,
            "server_error" => $error
        ];
    }

    public function checkPaymentStatus($paymentRequestId) {
        $result = false;
        
        $traceId =  substr(uniqid(rand(), false), 4, 8) . "-" . 
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 12);

        $request_time = gmdate("Y-m-d\TH:i:s\Z");
        
        $headers = array(
            "(request-target)"  => "get /payments/paymentrequests/$paymentRequestId",
            "Api-Version"       => self::VERSION,
            "Request-Date-Time" => $request_time,
            "Trace-Id"          => $traceId, // user id + company id
            "Authorization"     => "Bearer $this->access_token"
        );
        $signature = $this->generateSignature($headers);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $this->api_base_url."/payments/paymentrequests/$paymentRequestId",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "GET",
            CURLOPT_HTTPHEADER      => array(
                "Api-Version: ".self::VERSION,
                "Content-Type: application/json",
                "Accept: application/json",
				"Accept-Language: en-US",
                "Trace-Id: $traceId",
                "Request-Date-Time: $request_time",
                "Signature: $signature",
                "Authorization: Bearer $this->access_token"
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        $decoded_response = array();
        if($response) {
            $this->logging($response, "checkPaymentStatus");
            $decoded_response = json_decode($response, true);
        }
        if($error) {
            $this->logging($error, "checkPaymentStatus");
        }
        return [
            "result" => empty($error) && !isset($decoded_response["errors"]), 
            "data" => $decoded_response, 
            "errors" => isset($decoded_response["errors"]) ? $decoded_response["errors"]:null,
            "server_error" => $error
        ];
    }

    public function cancelPayment($paymentRequestId) {
        $result = false;
        
        $traceId =  substr(uniqid(rand(), false), 4, 8) . "-" . 
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 4) . "-" .
                    substr(uniqid(rand(), false), 0, 12);

        $request_time = gmdate("Y-m-d\TH:i:s\Z");
        
        $headers = array(
            "(request-target)"  => "put /payments/paymentrequests/$paymentRequestId/cancel",
            "Api-Version"       => self::VERSION,
            "Request-Date-Time" => $request_time,
            "Trace-Id"          => $traceId, // user id + company id
            "Authorization"     => "Bearer $this->access_token"
        );
        $signature = $this->generateSignature($headers);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $this->api_base_url."/payments/paymentrequests/$paymentRequestId/cancel",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "PUT",
            CURLOPT_HTTPHEADER      => array(
                "Content-Length: 0",
                "Api-Version: ".self::VERSION,
                "Content-Type: application/json",
                "Accept: application/json",
				"Accept-Language: en-US",
                "Trace-Id: $traceId",
                "Request-Date-Time: $request_time",
                "Signature: $signature",
                "Authorization: Bearer $this->access_token"
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        $decoded_response = array();
        if($response) {
            $this->logging($response, "cancelPayment");
            $decoded_response = json_decode($response, true);
        }
        if($error) {
            $this->logging($error, "cancelPayment");
        }
        return [
            "result" => empty($error) && !isset($decoded_response["errors"]), 
            "data" => $decoded_response, 
            "errors" => isset($decoded_response["errors"]) ? $decoded_response["errors"]:null,
            "server_error" => $error
        ];
    }
    
    public function vaidateSignature($headers) {
        if(!isset($headers["signature"])) {
            return false;
        }
        try {
            $sigStr = $headers["signature"];
            $sigStrArray = explode(",", $sigStr);
            $sigArray = array();
            foreach ($sigStrArray as $key => $value) {
                $element = explode("=", $value, 2);
                if(count($element) != 2) {
                    throw new \Throwable("");
                }
                $sigArray[$element[0]] = str_replace("\"", "", $element[1]);
            }
            
            $signingBase = "";
            $headersArray = explode(" ", $sigArray["headers"]);
            foreach ($headersArray as $key => $header) {
                if($signingBase !== "") {
                    $signingBase .= "\n";
                }
                $signingBase .= strtolower($header) . ": " . $headers[$header];
            }
            $hash = base64_encode(hash_hmac("sha256", $signingBase, base64_decode($this->signing_key), true));
            if($this->signing_key_id == $sigArray["keyId"] && $sigArray["signature"] == $hash) {
                return true;
            }
            return false;
        } catch (\Throwable $th) {
            return false;
        }
        
    }

    private function generateSignature($headers) {
        $signingBase = "";
        $header = "";
        foreach ($headers as $key => $value) {
            if($signingBase !== "") {
                $signingBase .= "\n";
                $header .= " ";
            }
            $signingBase .= strtolower($key) . ": " . $value;
            $header .= $key;
        }
        $hash = base64_encode(hash_hmac("sha256", $signingBase, base64_decode($this->signing_key), true));
        return "keyId=\"$this->signing_key_id\",algorithm=\"hmac-sha256\",headers=\"$header\",signature=\"$hash\"";
    }
    
    private function logging($data, $func = "") {
        if($this->log_file_path) {
            $dateTime = date("Y-m-d H:i:s");
            file_put_contents($this->log_file_path, "[$dateTime][$func] $data \n", FILE_APPEND);
        }
        return true;
    }

}


?>